<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerLangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_lang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image')->nullable();
            $table->string('alt')->nullable();
            $table->string('note')->nullable();
            $table->unsignedBigInteger('banner_id')->unsigned();
            $table->foreign('banner_id')->references('id')->on('banner')->onDelete('cascade');
            $table->unsignedBigInteger('lang_id')->unsigned();
            $table->foreign('lang_id')->references('id')->on('lang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_lang');
    }
}
