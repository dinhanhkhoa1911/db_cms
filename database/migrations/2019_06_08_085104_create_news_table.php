<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('author')->nullable();
            $table->dateTime('time_show')->nullable();
            $table->dateTime('time_hide')->nullable();
            $table->string('slug')->nullable();
            $table->string('origin')->nullable();
            $table->string('file')->nullable();
            $table->integer('access')->nullable();
            $table->string('status')->nullable();
            $table->string('featured')->nullable();
            $table->string('target')->nullable();
            $table->integer('viewed')->default(100)->nullable();
            $table->integer('position')->default(1)->nullable();
            $table->unsignedBigInteger('trend_id')->unsigned();
            $table->foreign('trend_id')->references('id')->on('trend');
            $table->unsignedBigInteger('site_id')->unsigned();
            $table->foreign('site_id')->references('id')->on('site');
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
