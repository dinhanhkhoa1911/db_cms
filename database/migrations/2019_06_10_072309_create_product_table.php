<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('serial')->nullable();
            $table->string('guarantee')->nullable();
            $table->string('expired')->nullable();
            $table->string('time_show')->nullable();
            $table->string('time_hide')->nullable();
            $table->string('file')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('feature')->nullable();
            $table->string('access')->nullable();
            $table->string('position')->nullable();
            $table->integer('viewed')->nullable();
            $table->unsignedBigInteger('site_id')->unsigned();
            $table->foreign('site_id')->references('id')->on('site');
            $table->unsignedBigInteger('producer_id')->unsigned();
            $table->foreign('producer_id')->references('id')->on('producer');
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
