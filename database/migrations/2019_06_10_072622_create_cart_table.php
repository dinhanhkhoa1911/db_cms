<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code_bill')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('ship_method')->nullable();
            $table->string('paytime')->nullable();
            $table->string('status_bill')->nullable();
            $table->integer('sub_total')->nullable();
            $table->integer('ship_fee')->nullable();
            $table->integer('tax')->nullable();
            $table->integer('total_price')->nullable();
            $table->mediumText('note_ship')->nullable();
            $table->mediumText('admin_note')->nullable();
             $table->unsignedBigInteger('shipper_id')->unsigned();
            $table->foreign('shipper_id')->references('id')->on('shipper');;

           $table->unsignedBigInteger('user_receive_cart_id')->unsigned();
            $table->foreign('user_receive_cart_id')->references('id')->on('user_receive_cart');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
