<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullname_personal')->nullable();
            $table->string('phone_personal')->nullable();
            $table->string('address_personal')->nullable();
            $table->string('ward_personal')->nullable();
            $table->string('district_personal')->nullable();
            $table->string('city_personal')->nullable();
            $table->string('facebook_personal')->nullable();
            $table->string('note_personal')->nullable();
            $table->integer('tax_code')->nullable();
            $table->string('company')->nullable();
            $table->string('address_company')->nullable();
            $table->string('ward_company')->nullable();
            $table->string('district_company')->nullable();
            $table->string('city_company')->nullable();
            $table->string('note_company')->nullable();
            $table->string('director')->nullable();
            $table->string('occupation')->nullable();
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_info');
    }
}
