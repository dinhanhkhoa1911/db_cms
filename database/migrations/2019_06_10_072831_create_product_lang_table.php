<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductLangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_lang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('price_origin')->nullable();
            $table->integer('price_buy')->nullable();
            $table->integer('price_discount')->nullable();
            $table->mediumText('intro')->nullable();
            $table->text('content')->nullable();
            $table->string('image')->nullable();
            $table->string('alt')->nullable();
            $table->string('youtube')->nullable();
            $table->string('slug')->nullable();
            $table->string('value');
            $table->string('meta_robots')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();

            $table->unsignedBigInteger('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('product');

            $table->unsignedBigInteger('lang_id')->unsigned();
            $table->foreign('lang_id')->references('id')->on('lang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_lang');
    }
}
